# A synchronous alternative is to use: LVM mirror in asymmetric 'writemostly' over a combination of Local + network blockdevice!
- Gluster, Sheepdog, Ceph
- This allows reads from high performance Local Raid!

## Limits:
- Cannot deal with Network Segmentation! only use on small LANs
- Slow network will reduce local write! Read remains unaffected.
