# Compiling
'''Further / more accurate / latest instructions may be found in README and in INSTALL. You
must not only install the kernel and the mars.ko kernel module to all of your bare metal cluster
nodes, but also the marsadm userspace tool.'''
- from: mars-user-manual.pdf


# Supported Kernel
4.9 in Debian [issues/21](https://github.com/schoebel/mars/issues/21)