We run MARS and LVM or ZFS in it's own minimalist "JeOS" Unikernel Xen Dom0, and export an 'nbd' across Paravirtual ringbuffer to a guest or host!
- Containers can run on Host! for minimum overhead
- Isolates storage from Applications
- Lets us use stable Kernels for MARS!
- Complete control over kernel config & size!